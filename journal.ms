.XS 1
Secret Societies

Secret Societies - Arkon Daraul
Reading Dates: 21/01/21 - Present

Foreword

“In Borneo, initiates of the hunting societies considered it meritorious and necessary to hunt heads. In Polynesia, infanticide and debauch were considered essential for initiation into societies where the tribal code needed members who indulged in these things, as pillars of society.”

There are initiation trials in Africa that “not infrequently resulted in death or madness of the candidate.” No further information given.

There is a “human desire to be one of the elect” that drives people to attempt to join secret societies.

Hassan-i Sabbah is known in the West as the Old Man of the Mountain. Founder of the Order of Assassins. 

“Today, the sect of the Hashishin (druggers) still exists in the form of the Ismailis (Ishmaelites), whose undisputed chief, endowed by them with the divine attributes is the Aga Khan.”

The Aga Khan is the Imam of the Shia Ismaili Muslims. This title is currently held by Prince Shah Karim al-Husseini, Aga Khan IV. All Aga Khans claim to be descendants of Muhammad.

Aga - Old Turkic and Mongolian meaning “elder brother.”
Khan - King/Ruler in Turkic and Mongolian languages.

In the seventh century AD, followers of Islam were split into two divisions: 
    • Orthodox - Regard Muhammad as the (sole?) bringer of divine inspirations; and
    • Shias - Consider Muhammad’s successor, Ali ibn Husayn Zayn al-Abidin (The Fourth Imam), to be “more important.”

The Shias are the focus of the text. For Shias, secrecy, organisation, and initiation were important to their survival. They believed they could eventually become the primary flavour of Islam despite smaller numbers.

The Shias started societies which practised secret rites in which the personality of Ali was “worshipped.” The Abode of Learning, based in Cairo, was one of the most successful of these societies. It was a “training ground for fanatics.” Members were told that they would obtain hidden power and timeless wisdom if they succeeded.
